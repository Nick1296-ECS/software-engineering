/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restclient;

import org.apache.cxf.jaxrs.client.WebClient;

/**
 *
 * @author biar
 */
public class Client {
    public static void main(String[] args) throws Exception {
        WebClient client = WebClient.create("http://localhost:7070/restserver");
        Course course = client.path("courses/1").accept("text/xml").get().readEntity(Course.class);
        
        
        System.out.println(course.getStudents());

    }
}
