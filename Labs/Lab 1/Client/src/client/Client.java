/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import edu.softeng.aaaws.ClientEntry;
import edu.softeng.aaaws.ClientsMap;
import edu.softeng.bankinterface.Operation;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author biar
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("up!");
     ClientsMap map= getClients();
     Iterator<ClientEntry> it=map.getClientEntries().iterator();
     
     while(it.hasNext()){
        ClientEntry entry= it.next();
        System.out.println(entry.getId()+ " "+entry.getClient().getName()+ " "+ entry.getClient().getSurname());
        List<Operation> o=getOperationByClientID(entry.getId());
        Iterator<Operation> ops=o.iterator(); 
        Operation op;
        while(ops.hasNext()){
            op=ops.next();
            if(op.getDescription().equals("benzina autostrada")){
                System.out.println(op.getDate()+ " "+op.getDescription()+ " "+op.getAmount());
            }
        }
     }
    }

    private static ClientsMap getClients() {
        edu.softeng.aaaws.AAAWSimplService service = new edu.softeng.aaaws.AAAWSimplService();
        edu.softeng.aaaws.AAAWS port = service.getAAAWSimplPort();
        return port.getClients();
    }

    private static java.util.List<edu.softeng.bankinterface.Operation> getOperationByClientID(int arg0) {
        edu.softeng.bankinterface.BankInterfaceimplService service = new edu.softeng.bankinterface.BankInterfaceimplService();
        edu.softeng.bankinterface.BankInterface port = service.getBankInterfaceimplPort();
        return port.getOperationByClientID(arg0);
    }

    
}
