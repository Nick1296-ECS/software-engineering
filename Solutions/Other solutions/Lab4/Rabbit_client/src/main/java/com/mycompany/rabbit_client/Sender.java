package com.mycompany.rabbit_client;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

/**
 * This class implements the producer of a service, and in particular sends 
 * a message to the server (with IP address 192.168.49.81)
 */
public class Sender {
    
    // Queue name to which connect
    private final static String QUEUE_NAME = "hello";
    
    public static void main(String[] argv) throws Exception {
        
        // Create the connection with the server through the ConnectionFactory
        // to negoziation of protocol and authentication
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.49.81"); // Server IP or name to which connect
        while(true){
            try (Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()){
                // We don't care about closure because they are managed by java.io.closable

                // Create the queue through which to send the messages
                channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                String message = ".....";
                // PERSISTEN_TEXT_PLAIN : add persistency of mesages
                channel.basicPublish("", QUEUE_NAME, 
                        MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");
            }
        }
  }
}