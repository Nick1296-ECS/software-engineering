/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.bankinterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author biar
 */
@XmlType( propOrder = { "id", "client_id", "amount", "date" , "description"} )
@XmlRootElement( name = "Operation" )
class Operation {
    private int client_id, id;
    private double amount;
    private Date date;
    private String description;
    
    public Operation(){}
    
    public Operation(int id, int cid,double amount, String d,String desc) throws ParseException{
        this.id=id;
        this.client_id=cid;
        this.amount=amount;
        this.date=(new SimpleDateFormat("yyyy-mm-dd")).parse(d);
        this.description=desc;
    }

    public int getClient_id() {
        return client_id;
    }
    
    @XmlElement(name="client_id")
    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public int getId() {
        return id;
    }

    @XmlElement(name="id")
    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    @XmlElement(name="amount")
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    @XmlElement(name="date")
    @XmlJavaTypeAdapter(DateAdapter.class)
    public void setDate(Date date) {
        this.date = date;
    }
    
    public String getDescription() {
        return description;
    }

    @XmlElement(name="description")
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Operation{" + "client_id=" + client_id + ", id=" + id + ", amount=" + amount + ", date=" + date + ", description=" + description + '}';
    }
}
