/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;


import java.util.Properties;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author biar
 */
public class Client_connection {
    
    public static void main(String[] args) throws NamingException, JMSException{
        Context jndiContext = null; 
        ConnectionFactory connectionFactory = null; 
        Connection connection = null; 
        Session session = null; 
        Destination destination = null; 
        String destinationName = "dynamicTopics/Ordini"; 

        
        /* 
         * Create a JNDI API InitialContext object 
         */ 

        Properties props = new Properties(); 
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY,"org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        props.setProperty(Context.PROVIDER_URL,"tcp://192.168.49.81:61616"); 
        jndiContext = new InitialContext(props);
        
        connectionFactory = (ConnectionFactory)jndiContext.lookup("ConnectionFactory"); 
        destination = (Destination)jndiContext.lookup(destinationName); 
        
        connection = (Connection) connectionFactory.createConnection(); 
        Session sessionProd = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Session sessionCons = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
        
        MessageProducer producer=sessionProd.createProducer(destination);
        
        Message m=sessionProd.createTextMessage();
        
        
        m.setStringProperty("Utente", "M");
        m.setStringProperty("Nome", "Oracle");
        m.setFloatProperty("Prezzo", (float) 12.8);
        m.setIntProperty("Quantita", 6 );
        
        producer.send(m);
        
        
        
        
        MessageConsumer consumer=sessionCons.createConsumer(destination, "Utente = 'M'");
        
        connection.start();
        //Message m=consumer.receive();
        
        
        //listener registration
        MessageListener listener= new Listener();
        consumer.setMessageListener(listener);
        
       
    }
    
}
