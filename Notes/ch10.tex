% !TEX root= SE\ Notes.tex
\graphicspath{{./imgs/ch10/}}
\chapter{Effort Estimation}
Effort estimation is the process which allows to determine the cost, effort and time needed for a software project
given its requirements.
To proceed in effort estimation the step performed are:
\begin{enumerate}
	\item Requirements collection
	\item Function points estimation
	\item Lines of code estimation
	\item Time and effort estimation
	\item Cost estimation
\end{enumerate}
In general a project with many function points is likely to be abandoned or overdue, in opposition to a project that
has fewer function points which will be likely to be completed in time.

\section{Constructive Cost Model}
The CoCoMo \cite{CoCoMo} is a cost model based on the waterfall development process that relies on statistics to
estimate effort and
cost for a software project, is composed on three models which have slightly different formulas to estimate effort
according to the project phase in which effort needs to be estimated:
\begin{itemize}
	\item Analysis and planning
	\item Design
	\item Development
	\item Integration and test
\end{itemize}
In detail the estimation regards several factors:
\begin{description}
	\item[Effort, Cost (M):] man time require to develop the project [man-years,man-weeks..]
	\item[Delivery time (T):] Time required to deliver working software (with work parallelization [years, weeks..]
	\item[Manpower:] Number of people working during the project [$\frac{M}{T}$]
\end{description}
These parameters are adjusted according to the context in which the software is developed, which is represented by
other parameters that are measured in a scale of six values.
\subsection{1981 model}
In the 1981 there are three models to be used according to the difficulty of the project, and the project can be
estimated only in its dimension or according to the correction factors described above (represented here as $c_i$).
If we evaluate only the project dimension we have the following formulas with $S_k$ as the estimated lines of code,
assuming that the requirements do not change:
\begin{description}
	\item[Simple:] $M=2.4 \cdot S_k^{1.05}$ and $T=2.5 \cdot M^{0.38}$
	\item[Intermediate:] $M=3.0 \cdot S_k^{1.12}$ and $T=2.5 \cdot M^{0.35}$
	\item[Complex:] $M=3.6 \cdot S_k^{1.2}$ and $T=2.5 \cdot M^{0.32}$
\end{description}
If we consider the global correction coefficient we obtain:
\begin{description}
	\item[Simple:] $M=3.2 \cdot S_k^{1.05} \cdot \prod_1^{15} c_i$ and $T=2.5 \cdot M^{0.38}$
	\item[Intermediate:] $M=3.0 \cdot S_k^{1.12} \cdot \prod_1^{15} c_i$ and $T=2.5 \cdot M^{0.35}$
	\item[Complex:] $M=2.8 \cdot S_k^{1.2} \cdot \prod_1^{15} c_i$ and $T=2.5 \cdot M^{0.32}$
\end{description}
In this analysis the time of planning and analysis of the requirements is not considered, we consider a
man-month (MM), which is the unit of measure of $M$ to be composed of 152 hours (19 days of 8 hours), $T$ instead is
represented in months.
As a general trend, the more lines of code in the project, the more time is spent in the testing and coding phases.
To represent the phases duration in the project we use a Gantt chart\cite{gantt}.
As we said before the 1981 model uses several assumptions:
\begin{itemize}
	\item Waterfall model
	\item Stable requirements
	\item Adequate personnel
	\item Project management
	\item less than 28\% of error in 68\% of estimates
\end{itemize}
\subsection{CoCoMo II}
The second version of the CoCoMo \cite{CoCoMo2} model was born to address the limitation of the 1981 version and
to consider software reuse, different lifecycles and different levels of precision in the estimations.
This new version consists of two models:
\begin{description}
	\item[Early Design model:] there is a low level of detail in this model and few parameters (7), it's used in the
	initial phases of the project and uses function points to estimate the effort.
	\item[Post-Architecture model:] has more detail and parameters (17) than the early model and evaluates the effort
	taking into account function points and reuse; it's used for the development and maintenance phases.
\end{description}
These two models share 5 scaling factors for computing the exponents factors.
\paragraph{CoCoMo II formulas.}
Let $SCED$ be the required development schedule, $n$ be either $6+SCED$ or $16+SCED$, $EM_i$ be the effort
multipliers that adjust the model according to the environment and $A$,$B$,$C$,$D$ constants.
We compute the person months, $PM$, in the following way:
\begin{equation*}
	PM=A \cdot S^E \cdot \prod_i^n EM_i
\end{equation*}
Where $S$ is the estimated size of the project in KLOC.

The PM computation requires the computation of the economy/diseconomy of scales, $E$ (in CoCoMo 81 there were only
diseconomies), where $SF_j$ are the five scale factors that CoCoMo II uses:
\begin{equation*}
	E= B+0.01 \cdot \sum_{j=1}^5 SF_j
\end{equation*}
The development time,$T$, is computed as follows:
\begin{equation*}
	T= C \cdot PM^F \cdot SCED/100
\end{equation*}
\begin{equation*}
	F=D+0.2(E-B)
\end{equation*}
\paragraph{Scale Factors.}
The Scale factors are (Fig. \ref{SF}):
\begin{description}
	\item[Precendentedness (PREC):] familiarity with the work, given by past similar works, it's intrinsic to the project
	and uncontrollable. (Fig. \ref{PREC})
	\item[Development Flexibility (FLEX):] Flexibility and relaxation during work, intrinsic to the project and
	uncontrollable (Fig. \ref{FLEX})
	\item[Architecture / Risk Resolution (RESL):] Combines design thoroughness and risk elimination strategies included
	in the project (Fig. \ref{RESL})
	\item[Team Cohesion (TEAM):] Sources of project turbulence given by difficulties in synchronizing the stakeholders
	(difficulties created by people involved in the project) (Fig. \ref{TEAM})
	\item[Process Maturity (PMAT):] Process maturity level measured according to the CMM-levels (Fig.
	\ref{PMAT}). If CMM-levels are not available the EPML (Estimated Process Maturity Level) is computed as the percentage
	of compliance to the 18 Key Process Area goals defined by CMM, according to the following formula: $EPML=5 -
	[(\sum_{i=1}^n \frac{KPA\%_i}{100})\cdot \frac{5}{18}]$.
\end{description}
\begin{figure}[ht]
	\includegraphics[width=10cm]{SF}
	\caption{Scale Factor Values, $SF_j$, for COCOMO II Models}
	\label{SF}
\end{figure}
\begin{figure}[p]
	\includegraphics[width=10cm]{PREC}
	\caption{Precedentedness Rating Levels}
	\label{PREC}
\end{figure}
\begin{figure}[p]
	\includegraphics[width=10cm]{FLEX}
	\caption{Development Flexibility Rating Levels}
	\label{FLEX}
\end{figure}
\begin{figure}[p]
	\includegraphics[width=10cm]{RESL}
	\caption{RESL Rating Levels }
	\label{RESL}
\end{figure}
\begin{figure}[p]
	\includegraphics[width=10cm]{TEAM}
	\caption{TEAM Rating Components}
	\label{TEAM}
\end{figure}
\begin{figure}[p]
	\includegraphics[width=10cm]{PMAT}
	\caption{PMAT Ratings for Estimated Process Maturity Level (EPML)}
	\label{PMAT}
\end{figure}
\paragraph{Effort multipliers.}
We have 17 effort multipliers that can be used in the second model an only 7 of them can be used in the first model,
these multipliers are divided in different categories (Fig. \ref{EM}):
\begin{description}
	\item[Product]:

	\begin{description}
		\item[RELY:] Required product reliability
		\item[DATA:] Database size
		\item[CPLX:] Product complexity
		\item[RUSE:] Intended reuse of software models
		\item[DOCU:] Level of required documentation
	\end{description}
	\item[System]:

	\begin{description}
		\item[TIME:] Execution time constraints
		\item[STOR:] Main storage constraint
		\item[PVOL:] Platform volatility (frequency of changes)
	\end{description}
	\item[Personal]:

	\begin{description}
		\item[ACAP:] Analyst capability
		\item[PCAP:] Programmer capability
		\item[APEX:] Application experience
		\item[PLEX:] Platform experience
		\item[LTEX:] Language and tool experience
		\item[PCON:] Personnel continuity
	\end{description}
	\item[Project]:

	\begin{description}
		\item[SITE:] Multisite development
		\item[TOOL:] Use of software tools
		\item[SCED:] Schedule constraints
	\end{description}
\end{description}
\begin{figure}[ht]
	\includegraphics[width=10cm]{EM}
	\caption{Summary of all the Effort multipliers}
	\label{EM}
\end{figure}
All these multipliers make both models very variable when determining the effort needed to develop the project, even if
the models have different multipliers (Fig. \ref{7-17}).
\begin{figure}
	\includegraphics[width=10cm]{7-17}
	\caption{Correspondence between models multipliers}
	\label{7-17}
\end{figure}

\subsection{Software Reuse}
We need to model software reuse since it is not a trivial process, due to the fact that the code we want to reuse can
be subject to some modifications and has a certain level of familiarity, readability and documentation.
To take into account all these factors we use a non linear module which models the effort to adapt a an existing module
as the effort to develop a new module, measuring the equivalent lines of code (ESLOC).
This model is based on two aspects:
\begin{itemize}
	\item The complexity to adapt software which is derived from:
	\begin{description}
		\item[Software understanding (SU):] how the software is easy to read, understand and modify to be used in the new
		project in terms of documentation, readability and modularity of the code (from low to high: $[50,40,30,20,10]$)
		\item[Assessment and assimilation (AA):] if the code can be useful for the application and how it
		documentation can be integrated with the actual product through test, evaluation to process and documentation that
		needs to be written. (from none to extensive: $[0,2,4,6,8]$)
	\item[Programmer unfamiliarity (UNFM):] with the software to be integrated (from familiar to unfamiliar:
		$[0,0.2,0.4,0.6,0.8,1]$)
	\end{description}
	\item The percentage of modification, the Adapting Adjusting Factor (AAF)
	\begin{description}
		\item[DM:] percentage of modified design
		\item[CM:] percentage of modified code
		\item[IM:] percentage of the modification of the integration effort without reusing code
	\end{description}
\end{itemize}
We can compute the Equivalent SLOC in two ways, depending on the adapting adjusting factor (AAF).
\begin{equation*}
	AAF=(0.4 \cdot DM)+(0.3 \cdot CM)+(0.3 \cdot IM)
\end{equation*}
\begin{equation*}
	AAM=
	\begin{cases}
		\frac{[AA+AAF \cdot (1+(0.02 \cdot SU \cdot UNFM))]}{100} & \mbox{if $AAF \leq 50$}\\
		\frac{AA+AAF+(SU \cdot UNFM)}{100} & \mbox{if $AAF > 50$}
	\end{cases}
\end{equation*}
After having computed both the Adaptation adjustment factor and modifier, we have that the estimated KLOCs are:
\begin{equation*}
	EKLOC= AKLOC \cdot (1-\frac{AT}{100}) \cdot AAM
\end{equation*}
Where $AKLOC$ are the adapted lines of code that are modified to be of use in the actual project and $AT$ is the
percentage of code that is re-engineered by automatic translation.
With this parameter we can compute the relative cost and modification size required to reuse a piece of code in a
project (Fig. \ref{AAM})
\begin{figure}[ht]
	\includegraphics[width=10cm]{AAM}
	\caption{AAM values in best and worst cases}
	\label{AAM}
\end{figure}
\subsection{Backfiring}
After we have computed our function points we can compute with them the SLOC, in order to do that we can use tables,
which indicates the backfiring\footnote{The backfiring tables can be consulted from page 59 of CoCoMo slides.}
(The average correspondence between function points and SLOC according to the used language).
The LOC estimation can be done using the given data as follows:
\begin{itemize}
	\item 51 FP
	\item C Language
	\item Backfiring for C language = 128
	\item LOC = $ 51 \times 128 = 6528 = 6.528$ KLOC
\end{itemize}
\section{Proposed methodology}
With the CoCoMo II model we can estimate with a good approximation the time necessary to the delivery of the software
and the effort that we need to complete the project with a certain delivery time, so we can estimate the cost of the
project, tanking into account the cost that we sustain if we write new code and the cost that we have when we reuse
some modules. (Fig. \ref{method})
\begin{figure}[ht]
	\includegraphics[width=10cm]{method}
	\caption{Cost and effort estimation methodology}
	\label{method}
\end{figure}
