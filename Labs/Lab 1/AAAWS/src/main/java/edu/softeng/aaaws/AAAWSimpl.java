/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.aaaws;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.jws.WebService;
/**
 *
 * @author biar
 */
@WebService(endpointInterface= "edu.softeng.aaaws.AAAWS")
public class AAAWSimpl implements AAAWS{
    private Map<Integer,Client> clients= new LinkedHashMap<Integer,Client>();

    public AAAWSimpl() {
        clients.put(1, new Client("Massimo","Mecella"));
        clients.put(2, new Client("Miguel","Cerruti"));
    }
    
    
    
    @Override
    public Map<Integer,Client> getClients(){
        return clients;
    }
    
}
