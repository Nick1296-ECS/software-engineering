/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.aaaws;

import edu.softeng.aaaws.ClientsMap.ClientEntry;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author biar
 */
public class ClientsAdapter extends XmlAdapter<ClientsMap, Map<Integer,Client>>{

    @Override
    public Map<Integer, Client> unmarshal(ClientsMap v) throws Exception {
        Map<Integer,Client> map= new LinkedHashMap<Integer,Client>();
        List<ClientEntry> entries=v.getEntries();
        Iterator<ClientEntry> it= entries.iterator();
        ClientEntry entry;
        while(it.hasNext()){
            entry=it.next();
            map.put(entry.getId(), entry.getClient());
        }
        return map;
    }

    @Override
    public ClientsMap marshal(Map<Integer, Client> v) throws Exception {
        ClientsMap boundmap= new ClientsMap();
        Iterator<Map.Entry<Integer, Client>> it=v.entrySet().iterator();
        ClientsMap.ClientEntry boundentry;
        Map.Entry<Integer,Client> entry;
        while(it.hasNext()){
            entry=it.next();
            boundentry= new ClientsMap.ClientEntry();
            boundentry.setClient(entry.getValue());
            boundentry.setId(entry.getKey());
            boundmap.getEntries().add(boundentry);
        }
        return boundmap;
    }
    
}
