/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.aaaws;

import javax.xml.ws.Endpoint;

/**
 *
 * @author biar
 */
public class AAAWServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException{
       AAAWSimpl impl= new AAAWSimpl();
       String address="http://localhost:8080/AAAWS";
       Endpoint.publish(address,impl);
       Thread.sleep(60*1000);
       System.exit(0);
    }
    
}
