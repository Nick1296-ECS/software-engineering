/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.restclient;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author biar
 */
@XmlRootElement(name="BankAccount")
public class BankAccount {
    private Client holder;
    private List<Operation> operations;
    
    public BankAccount(String name,String surname){

        holder=new Client(name,surname);
        operations=new ArrayList<Operation>();
    }

    public BankAccount(){}

    public Client getHolder() {
        return holder;
    }

    @XmlElement(name="Holder")
    public void setHolder(Client holder) {
        this.holder = holder;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    @XmlElement(name="Operations")
    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.holder);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this.getClass() == obj.getClass() && this.getHolder().equals(((BankAccount)obj).getHolder())) {
            return true;
        }
        return false;
    }
    
    public void addOperation(int amount, String date, String desc) throws ParseException{
        operations.add(new Operation(operations.size()+1,amount,date,desc));
    }

    @Override
    public String toString() {
        return holder.toString();
    }
    
    
    
}
