/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.aaaws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author biar
 */
@XmlRootElement(name="ClientsMap")
public class ClientsMap {
    private List<ClientEntry> entries=new ArrayList<ClientEntry>();
    @XmlElement(nillable=false,name="ClientEntries")
    public List<ClientEntry> getEntries(){
        return entries;
    }
    
    @XmlRootElement(name="ClientEntry")
    public static class ClientEntry {
        private Integer id;
        private Client client;

        public Integer getId() {
            return id;
        }
        
        @XmlElement(name="id")
        public void setId(Integer id) {
            this.id = id;
        }

        public Client getClient() {
            return client;
        }
        @XmlElement(name="Client")
        public void setClient(Client client) {
            this.client = client;
        }
    
    }
}

