% !TEX root = SE\ Notes.tex
\graphicspath{{./imgs/ch2/}}
\chapter{Software Development Process Models}
\section{Software Development Process Modeling}
Software products are not tangible, so to manage a software project there are some specific methods.
Monitoring a software project is based on the definition of specific documents to be produced and the activities to
be performed. These documents allow to monitor and check the evolution of the project over time, providing means to
evaluate also its quality.
Every model has specific criteria to produce documentation and to perform activities; in general a good compromise is
to produce only the documentation that is useful for the project.

\subsection{Waterfall model}
\paragraph{Characteristics.}
In this model the specification and development phases are distinct.
In detail the model is composed of 5 phases: (Fig. \ref{waterfall})
\begin{enumerate}
	\item Requirements analysis and definition
	\item System and software design
	\item Implementation and unit testing
	\item Integration and system testing
	\item Operation and maintenance
\end{enumerate}
Each of these phases has to be complete before moving to the next phase.

\begin{figure}[ht]
 \includegraphics[width=10cm]{waterfall}
 \caption{Waterfall model flow between project phases}
 \label{waterfall}
\end{figure}

\paragraph{Drawbacks.}
This model does not permit changes after a phase has been completed. This causes an initial uncertainty because at the
beginning of the project the vision of the overall system is not clear.
The customer needs to wait until a working version of the system is ready, which happens near the end of the project.
These disadvantages lead to an high probability that at the end of the project the customer satisfaction is not
achieved.

\subsection{Process iteration}

\paragraph{Characteristics.}
In this model requirements always evolve in the course of the project so its common to have iterations where the
earlier project phases are reworked, moreover these iterations can be applied to any of the generic process models.
There are two related approaches: Incremental delivery and spiral development.

\subsubsection{Incremental delivery}
Has also two approaches, the prototypal model and the incremental model.

\paragraph{Prototypal model.}
This model is built on a working prototype (Fig. \ref{proto}) which implements only some basic functions; this is used
to disambiguate requirements before proceeding to implement the given requirements.
A prototype is created for each set of requirements, before actually implementing them in detail.

\begin{figure}[ht]
	\includegraphics[width=10cm]{proto}
 \caption{Prototypal model}
 \label{proto}
\end{figure}

\paragraph{Incremental model.}
This model is similar to the prototype model, however instead of using several prototypes it uses many incremental
versions (Fig. \ref{incremental}) of the product that are fully working to disambiguate the requirements.
Each version includes or modifies the functions of the previous ones, allowing a more accurate design of the whole
product.

\begin{figure}[ht]
 \includegraphics[width=10cm]{incremental}
 \caption{Incremental model}
 \label{incremental}
\end{figure}

\paragraph{Incremental development.}
Both the previous models are part of the incremental development and delivery model (Fig. \ref{incremental-d}), which
focuses on delivering a part of the required functionalities per cycle, prioritizing user requirements (the ones with
the highest priority are implemented first). Once the development of a set of requirements is started, the next
requirements need to wait the completion of the current development phase to be evaluated, developed and integrated.

\begin{figure}[ht]
	\includegraphics[width=10cm]{incremental-d}
 \caption{Incremental development workflow}
 \label{incremental-d}
\end{figure}

\paragraph{Advantages.}
By developing earlier the most important requirements we guarantee that they are the most tested in the system, since
every test tests the whole project, helping to delivery functionalities earlier and to increment customer value.
The incremental structure of the development makes the whole process able to clarify and elicit requirements that
cannot be made or specified in the early phases; this makes the overall risk of project failure lower.

\subsubsection{Spiral model}
This model represents the process as spiral (Fig. \ref{spiral}), where a phase in the process is represented by a
loop in the spiral.
Each loop is chosen depending on the requirements and risks are explicitly assessed ad resolved in each phase.

\begin{figure}[ht]
 \includegraphics[width=10cm]{spiral}
 \caption{Spiral development workflow}
 \label{spiral}
\end{figure}

The spiral model has several sectors which are all traversed in one loop:
\begin{description}
	\item[Objective setting:]
	Specific objectives for the phase are identified.
	\item[Risk assessment and reduction:]
	Risks are assessed and activities put in place to reduce the key risks.
	\item[Development and validation:]
	A development model for the system is chosen which can be any of the generic models.
	\item[Planning:]
	The project is reviewed and the next phase of the spiral is planned.
\end{description}


\subsection{Formal methods}
Formal methods focus on removing ambiguities in the requirement specification, by using formal languages (like Z,Z++)
with a specific and unambiguous syntax, allowing to avoid misunderstandings during the specification, development and
test phases.

\subsection{Extreme Programming}
This model is part of the Agile model family, so like all Agile models relies on user involvement during development,
constant code improvement  and delivering of small increment of functionalities.
This model is different from the other models in the Agile family because it relies on pairwise programming, a
technique where programmers code in couples (one codes while the other checks errors), to reduce mistakes and produce
better code.

\section{Process activities}

\subsection{Software specification (requirements engineering)}
In this phase functional and non-functional requirements are specified.
\begin{definition}
Functional requirements are requirements in where the functionalities of the services to be implemented are specified
\end{definition}
\begin{definition}
non-functional requirements are requirements where constrains on the system are specified.
\end{definition}
Quality requirements and performance requirements are an example of non-functional requirements.

The specified requirements must be analysed and validated and a feasibility study is performed, to understand
if the whole project in realizable or not. (Fig. \ref{req-eng})

\begin{figure}[ht]
 \includegraphics[width=10cm]{req-spec}
 \caption{Requirement engineering process}
 \label{req-eng}
\end{figure}

\subsection{Software design and implementation}
In this phase a software system is designed and realized, the design and the realization phase can be interleaved.

\paragraph{Design phase.}
A software structure is designed to accomplish the specifications, in detail the design involves:
\begin{itemize}
	\item Architectural design
	\item Interface design, both software interfaces and user interfaces
	\item Component design
	\item Data structure design
	\item Algorithm design
\end{itemize}

The design phase is documented as a set of models, mostly graphical, and the most used model to do so is the UML model.

\paragraph{Realization phase.}
The realization phase uses the results of the design phase to translate them into an executable program, removing most of the errors; programming however is creative activity and there is no generic programming process, so developers are
required to test their code and remove faults.

\subparagraph{Debugging process.}
In the debugging process developers locate the errors in their code, design the error repair procedure, repair the
error and re-test the program, to be sure that they haven't introduced other errors and that the discovered error has
been removed.
\subsection{Software validation and verification}
The validation of the system checks if it conforms to the specifications (validation) and meets the customer requirements (verification), the system is tested in with a sample of the real data it must handle.

\paragraph{Testing stages.}
In the testing phase there are several stages (Fig. \ref{test}), to ensure that the test is accurate:
\begin{description}
 \item[Unit testing.]
 The single components are tested independently (functions, objects or small coherent grouping of the two).
 \item[System testing.]
 The system as a whole is tested, to understand if interaction between units can cause problems.
 \item[Acceptance testing.]
 Test with customer data to be sure that the system meets the customer needs.
\end{description}


\begin{figure}[ht]
 \includegraphics[width=10cm]{test}
 \caption{Testing workflow}
 \label{test}
\end{figure}

\subsection{Software evolution}
Software is inherently flexible and can change because requirements can change due to changes in business
circumstances. The software that supports the business must also evolve and change as the business changes. (Fig.
\ref{evolution})
Although there has been a demarcation between development and evolution (maintenance), this is increasingly irrelevant
as fewer and fewer systems are completely new. Software evolution takes into account existing systems (other than the
one evolving) to plan the evolution of the current system.

\begin{figure}[ht]
	\includegraphics[width=10cm]{evolution}
	\caption{Software evolution workflow}
	\label{evolution}
\end{figure}
