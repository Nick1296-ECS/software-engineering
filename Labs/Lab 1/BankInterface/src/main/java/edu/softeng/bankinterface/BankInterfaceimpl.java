/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.bankinterface;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author biar
 */
@WebService(endpointInterface="edu.softeng.bankinterface.BankInterface")
public class BankInterfaceimpl implements BankInterface{

    private List<Operation> operations=new ArrayList<Operation>();
    
    public BankInterfaceimpl() throws ParseException{
        operations.add(new Operation(1,1,100,"2019-6-20","benzina autostrada"));
        operations.add(new Operation(2,2,125.3,"2014-3-6","benzina autostrada"));
        operations.add(new Operation(3,1,10,"2019-5-23","panino"));
        operations.add(new Operation(4,2,10.4,"2019-6-20","ricarica"));
    }
    
    @Override
    public List<Operation> getOperationByClientID(int ClientID) {
        Operation val;
        List<Operation> res= new ArrayList<Operation>();
        int elems=0;
        Iterator<Operation> it=operations.iterator();
        while(it.hasNext()){
            val=it.next();
            if(val.getClient_id() == ClientID){
                res.add(val);
            }
        }
        return res;
    }
    
}
