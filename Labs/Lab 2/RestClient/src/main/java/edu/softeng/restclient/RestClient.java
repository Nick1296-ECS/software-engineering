/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.restclient;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author biar
 */
public class RestClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        Client client = ClientBuilder.newClient();
        //creating a bank account
        WebTarget webtarget = client.target("http://localhost:8080/Bank/");
        Invocation.Builder ib = webtarget.request("text/xml");
        Response res;
        BankAccount acc=new BankAccount("Massimo","Mecella"),acc_res;
        res=ib.post(Entity.entity(acc, MediaType.TEXT_XML));
        System.out.println(res.getStatus() +" - " + res.getStatusInfo().getReasonPhrase());
        
        //visualizing the accounts
        List<Integer> ids=new ArrayList<Integer>();
        AccountEntries accounts= ib.accept(MediaType.TEXT_XML).get(AccountEntries.class);
        for(AccountEntry entry : accounts.getEntries()){
            System.out.println(entry.toString());
            ids.add(entry.getId());
        }
        
        //add an operation
        System.out.println(ids.get(0));
        webtarget= client.target("http://localhost:8080/Bank/"+ids.get(0)+"/operations");
        ib=webtarget.request("text/xml");
        res=ib.post(Entity.entity(new Operation(1,100.0,"2018-03-02","benzina autostrada"),MediaType.TEXT_XML));
        System.out.println(res.getStatus() +" - " + res.getStatusInfo().getReasonPhrase());
        
        //visualizing the operations
         webtarget= client.target("http://localhost:8080/Bank/"+ids.get(0)+"/operations");
        ib=webtarget.request("text/xml");
        Operations ops=ib.accept(MediaType.TEXT_XML).get(Operations.class);
        for(Operation op : ops.getOps()){
            System.out.println(op.toString());
        }
        
        //updating an account
        webtarget = client.target("http://localhost:8080/Bank/"+ids.get(0));
        ib = webtarget.request("text/xml");
        res=ib.put(Entity.entity(new BankAccount("Massimo","Cerruti"), MediaType.TEXT_XML));
        System.out.println(res.getStatus() +" - " + res.getStatusInfo().getReasonPhrase());
        
        //visualizing the accounts
        for(Integer i : ids){
        webtarget= client.target("http://localhost:8080/Bank/"+i);
        ib=webtarget.request("text/xml");
        acc_res=ib.accept(MediaType.TEXT_XML).get(BankAccount.class);
        System.out.println(acc_res.toString());
        for(Operation op: acc_res.getOperations()){
            System.out.println(op.toString());
        }
    }
        //deleting and account
        webtarget = client.target("http://localhost:8080/Bank/"+ids.get(0));
        ib = webtarget.request("text/xml");
        res=ib.delete();
        System.out.println(res.getStatus() +" - " + res.getStatusInfo().getReasonPhrase());
        
    }
    
}
