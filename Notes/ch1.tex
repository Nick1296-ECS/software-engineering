% !TEX root = SE\ Notes.tex
\graphicspath{ {./imgs/ch1/} }
\chapter{Standardization of software processes}

\section{Introduction to process models}
The main factors in software development are three:
\begin{itemize}
	\item People
	\item Technology
	\item Development process
\end{itemize}
Organizations try to improve the quality of their software by
improving their development processes.
\begin{definition}
	A process model is a structured collection of practices that describe the characteristics of effective processes
\end{definition}
Practices included into a process model are proven to be effective by experience.

A process model has many uses:
\begin{itemize}
	\item sets measurable objectives and priorities for a development process
	\item ensures stable, capable and measurable processes
	\item is a guide for improvement of project and organizational processes
	\item is used to diagnose and certify the state of current practices
\end{itemize}

\section{Capability Maturity Model Integration}
CMMs were initially developed for the American Department of Defense by the Software Engineering Institute in 1980, since then their popularity increased and they were added in modern CMMI.
\begin{definition}
	CMMI is a process improvement approach that provides organizations with the essential elements of effective processes.
\end{definition}
CMMI can be used either as a collection of best practices, each addressing a different area of interest, or as a
framework, organized in constellations (Fig. \ref{cmmi-const}), to prioritize and organize activities.

\begin{figure}[ht]
	\includegraphics[width=10cm]{cmmi-const}
	\caption{CMMI constellations}
	\label{cmmi-const}
\end{figure}

An Organization using CMMI has a capability level assigned, which starts from 0 and can go up to 5. (Fig. \ref{capab})
\begin{description}
	\item[0 - Incomplete.] This is not a CMMI level since the process has not been fulfilled in at least one of its goals.
	\item[1 - Performed.] The specific goals of the process area are fulfilled supporting the work to create the
required products.
	\item[2 - Managed.] The performed process has the basic
	infrastructure in place to support the
	process. It is planned and executed in
	accordance with policy; employs
	skilled people who have adequate
	resources to produce controlled outputs;
	involves relevant stakeholders; is
	monitored, controlled, and reviewed;
	and is evaluated for adherence to its
	process description. The process
	discipline reflected by capability level 2
	helps to ensure that existing practices
	are retained during times of stress.

	\item[3 - Defined.]  The managed process is tailored from the organization's set of standard
	processes according to the
	organization's tailoring guidelines, and
	contributes to the organizational
	process assets.

	\item[4 - Quantitatively Managed.] Defined process that is controlled using
	statistical and other quantitative techniques.
	Quality and process performance is understood in statistical terms and is managed throughout the life of the process.

	\item[5 - Optimizing.] Quantitatively managed process that is improved continually based
	on an understanding of the common causes of
	variation inherent in the process and through both incremental and innovative improvements.
\end{description}


\begin{figure}[ht]
	\includegraphics[width=10cm]{capab}
	\caption{Organization capability levels}
	\label{capab}
\end{figure}


All CMMI models have multiple process area (PA) (Fig. \ref{PA}) that have specific goals to satisfy (up to 4 different
goals) and practices, there are also generic goals that are common to all the process areas.

\begin{figure}[ht]
	\includegraphics[width=10cm]{areas}
	\caption{Process Areas}
	\label{PA}
\end{figure}

Each PA goals are not meant to be satisfied all at once, but each goal needs to be satisfied once a determined level is
reached. (Fig. \ref{Maturity})

\begin{figure}[ht]
	\includegraphics[width=10cm]{maturity}
	\caption{PAs in relation to the CMMI level}
	\label{Maturity}
\end{figure}

\section{ISO 12207 standard}
\begin{definition}
	The ISO 12207 standard for sotware lifecycle processes defines and structures all activities involved in a software
development process. It is based on a functional approach and its main goal is to provide a common language to involved
stakeholders.
\end{definition}

It is composed by: (Fig.\ref{12207-proc})
\begin{itemize}
	\item 5 lifecycle processes
	\item 8 supporting lifecycle processes
	\item 4 organizational processes
\end{itemize}
Each process is specified in terms of activities and has a specified set of outcomes.


The ISO 12207 standard is based on two fundamental properties: \textbf{modularity} and \textbf{responsibility}.
To make each module as independent as possible and to establish responsibility for each process; to facilitate
operation with many processes and many legal actors involved.

\begin{figure}[ht]
	\includegraphics[width=10cm]{iso12207-proc}
	\caption{ISO 12207 Processes}
	\label{12207-proc}
\end{figure}

\section{ISO 9000 standards for quality management systems}

The ISO 9000 family addresses "Quality management” and has several standards:

\begin{description}
	\item[ISO 9000:2015 Fundamentals and vocabulary.]
	Description of the core language for the complete ISO 9000 family.
	Also describes the seven principles of quality management with tips to ensure they are reflected in the work.
	It also contains term and definition for other standards in the family, like ISO 9001.
	\item[ISO 9001:2015 Requirements.]
	It is intended for being used in any organization which designs, develops, manufactures, installs
	and/or services any product or provides any form of service. It provides requirements necessary to achieve customer
satisfaction, the requirements for the continual improvement of the product/service. It is the target of the
certification process.

	\item[ISO 9004:2009 Guidance for Performance Improvement.]
	Guidelines to improve and already mature system, covering continual improvement.

	\item[ISO 19011:2012.] Guidance to perform internal and external audits to ISO 9001, preparing the organization to
seek an external certification.

\end{description}

The certification for ISO 9001 gives several advantages such as control over key processes, better efficiency,
continual improvement, effective risk management and potential world-wide recognition; however this certification comes
with several disadvantages such as the need for more documentation to be produced, is difficult to implement, it's too
abstract and it's costly to maintain and to obtain.

The ISO 9000 has several building blocks:
\begin{description}
	\item[Quality management system.]
	General documentation requirements that are the foundation of the management system, what documentation needs to be
	produced and how it must be controlled.
	It has also some general requirements for processes, defining how they must interact with each other, what resources
	are needed and how to measure and monitor the processes.
	\item[Management responsibility.]
	Top management rules, which comprehend:
	\begin{itemize}
		\item knowledge of the customer requirements at a strategic level and commitment to
		meet these requirements
		\item set policies and objectives
		\item plan to meet the objectives
		\item ensuring clear internal communication
		\item regular reviews of the management system
	\end{itemize}
	\item[Resource management.]
	Deals with people capabilities requirements and resource requirements to carry out tasks that will ensure customer
	satisfaction.
	\item[Product/service realization.]
	Deals with the process that produces the service/output.
	\item[Measurement, analysis, and improvement.]
	Measurements to take to make system measurable, which can monitor system performance with internal audits, process effectiveness and customer requirements satisfaction level to improve the system.
\end{description}

\subsection{ISO certification}
The certification is released by third-part certification bodies that are authorized by accreditation bodies, both of them take fees and ensure the world-wide recognition of the certification, which needs to be renewed usually every three years.

The ISO 9001 certification requires two documents (Fig. \ref{docs}) to describe each process:
\begin{description}
	\item[Quality manual.]
	General quality policy of the organization regarding a set of requirements and resources.
	\item[Quality policy.]
	Adaptation of the quality manual to a specific project
\end{description}

\begin{figure}[ht]
	\includegraphics[width=10cm]{qm-qp}
	\caption{Documentation scheme}
	\label{docs}
\end{figure}
