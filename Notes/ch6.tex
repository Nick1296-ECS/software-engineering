%! TEX root= SE\ Notes.tex
\graphicspath{{./imgs/ch6/}}
\chapter{Microservices}

Microservices \cite{wiki-micro,azure-micro,aws-micro,micro} are an architectural and organizational approach to
software development where software is composed of small independent services that communicate over well-defined APIs.
These services are owned by small, self-contained teams.

\section{Monolithic vs. Microservices Architecture}
\paragraph{Monolithic Architecture.}
During the client/server era, we tended to focus on building tiered applications by using specific technologies in each
tier. The term monolithic application has emerged to describe these approaches. The interfaces tended to be between the
tiers, and a more tightly coupled design was used between components within each tier.
Monolithic applications are often simpler to design, and calls between components are faster because these calls are
often over interprocess communication (IPC). Also, everyone tests a single product, which tends to be a more efficient
use of human resources. The downside is that there's a tight coupling between tiered layers, and you can't scale
individual components. If you need to do fixes or upgrades, you have to wait for others to finish their testing. It's
harder to be agile.

So with a monolithic architecture we have a more efficient system that is easy to manage with these disadvantages:
\begin{itemize}
	\item The architecture is hard to evolve
	\item Long time for each release (Bigger the system means longer build, test and release cycle)
	\item Long time to add new features (a new feature must be made compatible with all the existing ones)
\end{itemize}
These disadvantages origin from the tight coupling that the module has in the monolithic architecture and from the use
of shared libraries between many different services, this means that an update on these libraries requires the
adaptation of all the services that use these libraries. The same happens with data, which is likely to be
stored in a single database in several tables, permitting developers to create dependencies between tables and to stress
the DBMS.
An application of this type is scaled only by cloning it on multiple servers.

\paragraph{Microservices architecture.}

\begin{definition}
	Microservices is a service-oriented architecture composed of loosely coupled elements, each with a bounded context.
\end{definition}

Microservices are self-contained pieces of business functionalities with clear interfaces, and may, through its own
internal components, implement a layered architecture. Each one typically encapsulates a simpler business functionality,
which you can scale up or down, test, deploy, and manage independently.

Each microservice is independent from the others and it is developed by a dedicated team, so the updates of a
microservice are independent from the other microservices since the only requirement for the communication is an
interface exposed over the network. (Fig. \ref{micro-mono})

One important benefit of a microservices approach is that teams are driven more by business scenarios than by
technology. Smaller teams develop a microservice based on a customer scenario and use any technologies that they want to
use.

This means that there is no need for standardized tech to be used in the whole organization, however a set of preferred
technologies is recommended.

\begin{figure}
	\includegraphics[width=10cm]{micro-mono}
	\caption{Microservices and Monolithic architecture comparison}
	\label{micro-mono}
\end{figure}

Each microservice has a small data storage (a database with the chosen technology for the service), an application
level and a public API exposed over the network, to avoid coupling with other services.

When you use a microservices approach, you compose your application of many small services. These services run in
containers that are deployed across a cluster of machines. There is also an orchestrator that has the job of
coordinating all these services to execute correctly your application logic.

\section{Microservices Characteristics}
Microservices follow some basic principles:

\paragraph{Reliance only on the public API (Rely on the public API).}
This allows to simplify and decouple the communication from the application logic and the format
in which data is stored, however the API must evolve in a backward compatible way, to give the other services the time
to adapt to the API changes.

\paragraph{Using of the best tool for the each microservice (Use the right tool for the job).}
In a microservice we can (and should) use the best technology we can afford to accomplish the scope of the service,
since other services can see only the API, moreover we can also change the used technologies as we like without
worries, since we must only maintain the API functionalities.

\paragraph{Services must be secured (Secure your services).}
Communication over the network should not be unprotected, even if it is between microservices (since the can be
scattered around the word), so there are several means to protect the data and the availability of microservices like
API throttling, sandboxes, gateways, authorization , authentication and sessions/secrets management.

\paragraph{Microservices should create a good ecosystem (Be a good citizen within the ecosystem).}
Developing several independent microservices is done to increase the efficiency of an application or service and we must
keep in mind that these services are not visible to the end user, so we must define the requirements for each service
in a way that they are compatible and compliant with the application SLA and the user expectancies.
To do so we need to log each microservice to understand if it is meeting the requirements and we should also create an
application log to see what and where there can be problems when all the microservices are used as an ecosystem.

\paragraph{More than a technology transformation.}
Shifting from the monolithic to the microservices architecture will likely have and impact in the organization's
structure, since now there are small teams that need the same things that were needed by the monolithic application, so
there cannot be anymore functional teams (e.g. a team composed only of UI specialists), since the need for a specific
functionality is now distributed across all the microservices.
This also means that teams are smaller and there is better accountability and focus on the scopes of each functionality
since each team owns a microservice.

\paragraph{Automation (Automate everything).}
The best way to increase the productivity with microservices is to use an infrastructure, like Amazon AWS or Microsoft
Azure, because these infrastructure provide several functionalities to automate the deployment processes, the scaling
and the maintenance of each microservice.

So the use of microservices has several benefits, even if it is not applicable everywhere:
\begin{itemize}
	\item Easier the to maintain and evolve (we can evolve each component independently)
	\item Faster Build/Test/Release cycle
	\item Easy to add new functionalities (the accountability and ownership is clear)
\end{itemize}
