% !TEX root = SE\ Notes.tex
\graphicspath{{./imgs/ch3/}}
\chapter{An Introduction to Scrum}
Scrum is an agile process that allows us to focus on delivering the highest business value in the shortest time.
It allows us to rapidly and repeatedly inspect actual working software (every two weeks to one month).
The business sets the priorities. Teams self-organize to determine the best way to deliver the highest priority
features. Every two weeks to a month anyone can see real working software and decide to release it as is or
continue to enhance it for another sprint.
This methodology is currently used by several organizations (like IBM, Microsoft and Google) to produce produce
different products (ISO 9001 certified products, software for several applications etc.).

\paragraph{The Agile Manifesto.}
Scrum is part of the Agile family of processes, so it is compliant with the Agile manifesto, which has the following
key concepts:

\begin{itemize}
	\item Individuals and interactions over process and tools;
	\item Working software over comprehensive documentation;
	\item Customer collaboration over contract negotiation;
	\item Responding to change over following a plan;
\end{itemize}

Each project, however has a "noise level" (Fig. \ref{noise}) which depends on the level of agreement on the requirements and on
the availability of the needed technologies.

\begin{figure}[ht]
	\includegraphics[width=10cm]{noise}
	\caption{Project noise level}
	\label{noise}
\end{figure}

Scrum as a process model has several characteristics, which are compliant with the Agile family of processes.
\begin{itemize}
	\item Self-organizing teams
	\item Product progresses in a series of 2-4 week “sprints”
	\item Requirements are captured as items in a list of “product backlog”
	\item No specific engineering practices prescribed
	\item Uses generative rules to create an agile environment for delivering projects
	\item One of the “agile processes”
\end{itemize}

The scrum process is also scalable, since it to organize processes that require many people several team can be created
and other scrum teams can be used to organize them, creating scrums of scrums.

\paragraph{Sprints}
Scrums processes make progress in a series of sprints (Fig. \ref{scrum}), which are similar to the iteration in other
processes, however they have typical duration which is 2-4 weeks; in this time frame a product must be designed,
implemented and tested, this time constraint is thought to give rhythm to the team.

\begin{figure}[ht]
	\includegraphics[width=10cm]{scrum}
	\caption{The workflow and the artifacts used and produced in a sprint}
	\label{scrum}
\end{figure}


Sprints make scrum teams do a little of every phase for each sprint and in general during a sprint requirements cannot
be changed, so the duration of the sprint must take into account how long the team can avoid changes while implementing
a part of the project.

\section{Scrum Framework}
A scrum process has a certain framework which is composed of a set of roles, some activities to be performed (called
ceremonies) and documentation to be produced (called artifacts).

\subsection{Roles}
\paragraph{Product Owner.}
Represents the management interest in the success of the project, it defines the features, content and release date of
the product. It is responsible for the project return of investment, can accept or reject work results and prioritizes
the features according to their market value, at each sprint.

\paragraph{Scrum master.}
Represents management to the project and is responsible for enacting Scrum values and practices into the team.
It also removes impediments and ensures that the team is fully functional and productive, enabling close cooperation
across all roles and functions.
Finally it shields the team from external interferences, making it focus on the sprint.

\paragraph{Team.}
The team members cannot change when a sprint is in progress and are general 5 to 9 people\footnote{The right number to
share a pizza}, they all should be full-time employed to guarantee their commitment to the project and should have
different capabilities (programmers, designers ,testers etc.).
Teams are self-organizing.

\subsection{Ceremonies}
\paragraph{Sprint planning.}
The sprint planning is composed of two events:
\begin{description}
	\item[Sprint prioritization.] where the product backlog is evaluated and the sprint goal is selected
	\item[Sprint planning.] where the design to reach the sprint goal is selected and the sprint backlog is created and
	estimated from the items chosen from the product backlog.
\end{description}
In the sprint backlog every task has an estimated duration, and all the tasks have an high level design.
The whole planning is done collaboratively by all the team.

\paragraph{Daily scrum meeting.}
It's a daily meeting of about 15 minutes\footnote{In front of the coffee vending machine} where there is a review by
all the team of what has been done until now, what are the problems and what else needs to be done. The meeting is
intended as a commitment among peers.

\paragraph{Sprint review.}
An informal meeting where the team presents the sprint results, usually it involves only a demo, with no slides.

\paragraph{Sprint retrospective.}
After the sprint review there is this last meeting where the team focuses on what where the problems in the last
sprint, how to address them and what instead should be repeated or introduced since it has a positive effect on the
sprint. This meeting usually doesn't last more than 30 minutes.

\subsection{Artifacts}
\paragraph{Product backlog}
It is the list of the desired work on a project (Fig. \ref{backlog}), organized in items, which are prioritized at the
beginning of each sprint by the product owner according to the value they have to the users of the product.

\begin{figure}[ht]
	\includegraphics[width=10cm]{backlog}
	\caption{A real product backlog}
	\label{backlog}
\end{figure}

\paragraph{User stories}
User stories are the Agile equivalent of use cases, they are 3x5" cards\footnote{it's the first used format, a post-it,
used by Connextra} where a goal is represented, with the following format:
\begin{description}
	\item[As a] [role]
	\item[I want to] [goal]
	\item[So that I can] [reason/purpose]
\end{description}
Each of these cards has a value which is agreed on by the team, representing the perceived implementation difficulty
of the story.
The reason behind these format is that in this way the appear less complicated\footnote{and scary, bhohohohoho} to the
customers, which can be involved directly in the product functionality description; they are also easy to rearrange
during development.
\paragraph{Sprint backlog}
Team members sign up for work of their own choosing, the estimated work remaining is updated daily and any team member
can modify the sprint backlog (Fig. \ref{s-backlog}).
Work for the sprint emerges as the sprint goes on. If work is unclear, define a sprint backlog item with a larger amount of time
and break it down later, so work is updated as it becomes known.

\begin{figure}[ht]
	\includegraphics[width=10cm]{s-backlog}
	\caption{A sprint backlog example}
	\label{s-backlog}
\end{figure}

\paragraph{Burndown chart.}
A display of what work has been completed and what is left to complete, for each developer or work item.
It is updated every day with the remaining amount of estimated hours needed to complete the sprint.

\begin{figure}[ht]
	\includegraphics[width=10cm]{burndown}
	\caption{Burndown chart example}
	\label{burndown}
\end{figure}

\paragraph{Release burndown chart.}
A variation of the burndown chart where the overall progress is shown, it is updated at end of each sprint.
