% !TEX root= SE\ Notes.tex
\graphicspath{{./imgs/ch7/}}
\chapter{Measurements and statistics}
Measures, in the field of Software Engineering, are meant to monitor a software project in various aspects:
(Fig.\ref{monitoring})
\begin{itemize}
	\item Verifying how far quality parameters are from reference values
	\item Identifying deviations from temporal and resource allocation planning
	\item Identifying productivity indicators
	\item Validating the effect of strategies aimed at improving the development process (quality, productivity, planning,
	cost control)
\end{itemize}
\begin{figure}[ht]
	\includegraphics[width=10cm]{measure}
	\caption{Monitoring a software project according to the selected criteria}
	\label{monitoring}
\end{figure}
After having took the needed measures we need to map them in a reference scale, which will provide a qualitative
rating of the project performance (Fig. \ref{qualitative}), because measures themselves do not provide any quality
judgment.
\begin{figure}[ht]
	\includegraphics[width=10cm]{qualitative}
	\caption{Mapping of measurements to a qualitative scale}
	\label{qualitative}
\end{figure}

\section{Basics of measure theory}
For our purpose we consider only five different measure scales (Fig. \ref{scales}):
\begin{description}
	\item[Nominal scale:] Classifies object into two categories, where members of the same category share some
		characteristics and each element can be only in one category (operators supported $\{=, \neq \}$).
	Its average values is meaningful only with frequency analysis on elements that appear in a certain category.
	\item[Ordinal scale:] It allows us to classify and rank objects in the scale, but we still don't know the distance
		between any pair of ranks (operators supported $\{=, \neq, >, <\}$).
		In some ordinal scales the distance between items is assumed to be the same; an average does not have always sense
		in this scale.
	\item[Interval scale:] An interval scale allows us to classify and rank items, knowing the sizes of differences
	between them, they normally have an arbitrary minimum and maximum and are characterized by having equal intervals
	between ranks. A scale like this also needs the definition of the used unit measure and cannot measure the magnitude
	between two values, since the zero point is not absolute (operators supported $\{=, \neq , >, <, +, -\}$).
	\item[Ratio scale:] A interval scale with an absolute zero value, to support magnitude comparison (operators supported
		$\{=, \neq, >, <, +, -, *, /\}$).
	\item[Absolute scale:] Ratio scale without values below zero.
\end{description}
The scale to be used in measurement must be chosen according to the characteristics of the objects we are measuring.
\begin{figure}[ht]
	\includegraphics[width=10cm]{scales}
	\caption{Comparison between the various scales}
	\label{scales}
\end{figure}

\subsection{Types of measures}
\begin{definition}
	Ratio: division between two values that come from two different and disjoint domains.
\end{definition}
Ratio is usually multiplied by 100 but is not a percentage, (e.g. males/females).
\begin{definition}
	Proportion: division between two values where the dividend contributes to the divisor.
\end{definition}
It's a value in $[0,1]$ (e.g. a/(a+b)).
\begin{definition}
	Percentage: a proportion or fraction expressed normalizing the divisor to 100.
\end{definition}
It should be avoided if values are below 30-50\%
\begin{definition}
	Rate: Measures the changes of quantity in respect to a quantity on which it depends.
\end{definition}
\begin{definition}
	Working definition: A debatable, non ambiguous and measurable definition.
\end{definition}
A working definition is used to validate statements with the usage of measures and a statistical analysis, since a
statement can be not measurable (Fig. \ref{work-def}).
\begin{figure}[ht]
	\includegraphics[width=10cm]{work-def}
	\caption{Different levels of abstraction and their relations}
	\label{work-def}
\end{figure}

\subsection{Quality of a measure}
\paragraph{Reliability of a measure.}
The reliability of a measure is estimated with the value of $\sigma^2$ on repeated measures of the same value.
It indicates if the measure can be repeated and the consistency of it.
\paragraph{Validity of a measure.}
The best available approximation to the truth or falsity of a given inference, proposition or conclusion.
Combining validity and reliability of a measure we can tell how "good" was the measure we took.
\begin{figure}[ht]
	\includegraphics[width=10cm]{validity}
	\caption{How validity and reliability of a measure are used to evaluate the "goodness" of the measure}
	\label{validity}
\end{figure}
\paragraph{Errors in measuring}
We know (by experience) that if we measure the same object twice we could get different results, we model this
phenomenon by defining the measure in the following way:
\begin{definition}
	Measure: A measure is composed by the true value of the phenomenon and the total error experienced while
	taking the measurement.
	\begin{equation*}
		M=T+E_{tot}
	\end{equation*}
\end{definition}
The total error experienced is broken down in two components \cite{errors}:
\begin{description}
	\item[Systematic error:] Influences validity of the measure, happens every time we take the measure (e.g.
	instrument's error), taking several measures and computing the average reduces this type of errors.
	\item[Random error:] Influences reliability of the measure, has random behavior and it's unpredictable.
\end{description}
The reliability of the error is measured by computing the ratio between the variance of the computed measure and
the variance of the metric used.
\begin{equation*}
	\rho_m = \frac{var(T)}{var(M)}= \frac{var(M)-var(E_r)}{var(M)}=1-\frac{var(E_r)}{var(M)}
\end{equation*}
\paragraph{Correlation.}
Correlation indicates if two variables have some kind of linear relationship, it's a value between $[-1,1]$ and it is
$1$ is the variables have the same behavior, $-1$ the behavior of one variable is the opposite of the other and $0$ if
the variables have no linear relation. (Fig. \ref{correlation})
\begin{figure}[ht]
	\includegraphics[width=10cm]{correlation}
	\caption{Examples of data with positive (A), negative (B) and no correlation (C)}
	\label{correlation}
\end{figure}

\section{Statistics}
We analyze a population $M={x_1,..,x_n} \subset X$ elements

\subsection{Descriptive statistics}
With descriptive statistic consider $M$ to be our dataset, so we can identify several parameters:
\begin{definition}
	Mean: $\mu=(\sum_i^n x_i)/n$
\end{definition}
\begin{definition}
	Variance: $\sigma^2=[\sum_i^n (x_i-\mu)^2]/n$
\end{definition}
\begin{definition}
	Standard deviation: $\sigma=\sqrt{\sigma^2}$
\end{definition}
\begin{definition}
	Median: The middle data point in a population; requires an ordinal scale to be computed and it's equivalent to the
	second quartile.
\end{definition}
When finding the median, if we have an even number of points we could have two medians; if we are in an interval scale
we can take the average of the two.
\begin{definition}
	Percentile: value below which a certain percentage of the observations falls.
\end{definition}
\begin{definition}
	Quartile: one of the three values that divide the dataset into four equal parts.
\end{definition}
\begin{definition}
	Mode: the most recurring data point.
\end{definition}
\begin{definition}
	Range: distance between the smallest and largest data point.
\end{definition}
The frequency distribution of a set of values is obtained by indicating the frequency for each value or by diving the
range of values in buckets and counting the number of elements in each bucket.

To represent a dataset by its quartiles we use a boxplot\cite{box-plot} representation (Fig.
\ref{boxplot}) in this way we graphically know where is the center of the dataset.
\begin{figure}[ht]
	\includegraphics[width=10cm]{boxplot}
	\caption{Boxplot representing the frequency distribution of dataset}
	\label{boxplot}
\end{figure}

\subsection{Inferential statistics}
The parameters used in descriptive statistics are random variables which values are determined by the causality of the
sample. Typically samples have a Gaussian distribution so we can perform several estimations.
In inferential statistics we don't have all the data (as in descriptive statistics) so our parameters refer only to
the sample's elements and we want to use them, along with the distribution of the dataset to infer information on the
whole dataset, $X$.
\paragraph{Confidence interval.}
\begin{definition}
	Confidence interval: Probability that the mean of a population $X$ is in a finite interval centered on the mean of
	the sample of $n$ elements of the population.
\end{definition}
The size of the intervals determines the probability of error, which is proportional to the standard deviation an
inversely proportional to the size of the sample.
It allows to estimate a population parameter, by using an interval which is likely to include that parameter; the
confidence level will determine the reliability of the estimate. To increase the confidence level we must widen the
interval, to increase the number of possible values in the sample that are expected to include the true population
parameter (Fig. \ref{confidence}).
Let $x$ be the parameter value, the confidence interval then is $[x-d,x+d]$ with confidence level $p$.
\begin{figure}[ht]
	\includegraphics[width=10cm]{confidence}
	\caption{An example of a confidence interval}
	\label{confidence}
\end{figure}
\paragraph{Hypothesis verification.}
Often we need to compare different repeated measures, to do so we perform appropriate statistic tests.
\begin{definition}
	Null Hypothesis $H_0$ \cite{H0}: General statement that says that all true means are equal and the differences are random.
	This hypothesis is considered true until evidence indicates otherwise.
\end{definition}
\begin{definition}
	p-value \cite{p-value}: Probability that a random variable has a value greater that a given threshold if the null
	hypothesis is true. The value of the probability is computed as $\int_{x}^{+\infty} D(x)dx$ where $x$ is the measured
	value and $D$ is the distribution associated with the chosen random variable.
\end{definition}
The verification of $H_0$ uses a fixed error probability, $\alpha$.

We can test if two samples come from the same population.
To do so we use $H_0$ on one or more random variables, we also define two hypotheses:
\begin{description}
	\item[$H_0$]: samples come from the same population
	\item[$H_1$:] samples come from different populations
\end{description}
We need to define a formula which captures the difference between these data by generating a random variable and use
the p-value to check which hypothesis is true. The value of p is the probability to reject $H_0$ when is true.
We proceed like this:
\begin{itemize}
	\item $p \ge \alpha \Rightarrow$  we accept $H_0$
	\item $p \leq \alpha \Rightarrow$ we accept $H_1$
\end{itemize}
The value of $\alpha$ is fixed so we need to tune it according to how critical our decision is, because
critical decisions use little value of $\alpha$.
\begin{figure}[ht]
	\includegraphics[width=10cm]{dist}
	\caption{Distribution of the random variable used for computing the p-value (in red we have the reject zones)}
	\label{dist}
\end{figure}
\subsubsection{Student's t-test.}
Compares the mean values of two samples, with $n$ elements each.
\begin{equation*}
	t=\frac{\mu_a -\mu_b}{\sqrt{\frac{\sigma^2_a+\sigma^2_b}{n}}}
\end{equation*}
Assuming that a and b come from the same population we can compute the probability density of t and the probability of
t being equal or greater than a value.
This is expressed by a table that indicates the probability that $t \geq \alpha$, according to the degree of freedom
(sample dimension); given n elements and the mean we have n-1 degree of freedom for a single sample and the overall
number of degree of freedom is 2(n-1); also when reading the table we must remember that the $\alpha$ value given is
for a single tail of the distribution (e.g. if $\alpha=5\%$ then we must look at the table where $\alpha=2.5\%$). To
determine if we can accept $H_0$ we need to compare the computed t-value with the critical t-value given by the table
with the corresponding value of $\alpha$ and the same degree of freedom; if our value is less than the t-critical value
the we can accept $H_0$, otherwise we need to reject it.
\paragraph{Probability of error.}
\begin{itemize}
	\item$H_0$ is true:
	\begin{itemize}
		\item $H_0$ is rejected with probability $\alpha$ and we have a false positive
		\item $H_0$ is accepted with probability $1-\alpha$
	\end{itemize}
	\item $H_0$ is false:
	\begin{itemize}
		\item $H_0$ is rejected with probability $1-\beta$
		\item $H_0$ is accepted with probability $\beta$ and we have a false negative
	\end{itemize}
	However $\beta$ cannot be computed since it depends on the mean of the dataset, which is not known.
\end{itemize}

\subsubsection{Analysis of variance (ANOVA).}
Is a technique similar to the t-test to evaluate several ($\ge 2$) samples at once \cite{anova}.
We assume that all the samples are distributed by a Gaussian distribution.
The hypothesis we have are the following:
\begin{description}
	\item[$H_0$]: all samples have the same mean
	\item[$H_1$:] the are at least two different means between all the samples
\end{description}
\paragraph{Sum of squares.}
The first step in the ANOVA test is the computation of the variance within each sample, $SS_w$ and the variance between
all the samples $SS_b$.
Let $I$ be the number of samples, $J$ be the number of element in each samples, $Y_{ij}$ the $j$th element of the $I$th
sample and $\mu$ be the mean of all the elements in all the samples.
\begin{equation*}
	SS_w= \sum_{i= 1}^I \sum_{j=1}^J (Y_{ij}-\mu_i)^2
\end{equation*}
\begin{equation*}
	SS_b= J \cdot \sum_{i= 1}^I (\mu_i-\mu)^2
\end{equation*}
\paragraph{Fisher test.}
After having computed the variance parameter we can execute the Fisher test, by computing the F value:
\begin{equation*}
	F=\frac{\frac{SS_b}{I-1}}{\frac{SS_w}{I \cdot (J-1)}}
\end{equation*}
and proceed to the validation of the F value with the F-critical value as it is done with the t-test.
If the ANOVA test leads us to reject the $H_0$ hypothesis then we must test each couple of samples to find out in which
distribution each sample has.
\subsection{Examples of application}
We can use the t-test and ANOVA test to prove general statements in software production and testing.
For example we could use the ANOVA test to see if the ratio between tested KLOC and written KLOC increases then the
defect rate in the first year decreases, on several packages at once.
