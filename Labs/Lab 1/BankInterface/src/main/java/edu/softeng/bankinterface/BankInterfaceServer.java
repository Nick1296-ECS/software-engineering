/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.bankinterface;

import java.text.ParseException;
import javax.xml.ws.Endpoint;

/**
 *
 * @author biar
 */
public class BankInterfaceServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ParseException {
       BankInterfaceimpl impl= new BankInterfaceimpl();
       String address="http://localhost:8081/BankInterface";
       Endpoint.publish(address,impl);
       Thread.sleep(60*1000);
       System.exit(0);
    }
    
}
