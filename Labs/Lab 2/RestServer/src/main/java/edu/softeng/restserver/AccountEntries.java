/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.restserver;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author biar
 */
@XmlRootElement(name="AccountEntries")
public class AccountEntries {
    private List<AccountEntry> entries;
    public AccountEntries(){}
    
    public AccountEntries(List<AccountEntry> l){
        entries=l;
    }

    public List<AccountEntry> getEntries() {
        return entries;
    }

    @XmlElement(name="entries")
    public void setEntries(List<AccountEntry> entries) {
        this.entries = entries;
    }
    
    
}
