/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.bankinterface;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author biar
 */
public class DateAdapter extends XmlAdapter<String,Date>{

    @Override
    public Date unmarshal(String v) throws Exception {
        return (new SimpleDateFormat("yyyy-mm-gg")).parse(v);
    }

    @Override
    public String marshal(Date v) throws Exception {
        return v.toString();
    }
    
}
