% !TEX root= SE\ Notes.tex
\graphicspath{{./imgs/ch4/}}
\chapter{Distributed Programming}

\section{Client-Server Communication}

\paragraph{Layered architecture}

The layered architecture (Fig. \ref{layer}) separates layers of components from each other, giving it a much more
modular approach. A well known example for this is the OSI model that incorporates a layered architecture when
interacting with each of the components. Each interaction is sequential where a layer will contact the adjacent layer
and this process continues, until the request is been catered to.

\begin{definition}
	A client is any user or program that wants to perform and operation on the system.
\end{definition}

\begin{enumerate}
	\item Clients interact with the system through a presentation layer.
	\item The application logic determines what the system actually does.
	\item The resource manager deals with the organization (storage, indexing, and retrieval) of the data necessary to
	support the application logic.
\end{enumerate}

\begin{figure}[ht]
	\includegraphics[width=10cm]{layers}
	\caption{A layered distributed system}
	\label{layer}
\end{figure}
To indicate these layers there are several terminologies, in many cases the client concept includes also the
presentation layer.

\paragraph{Multitier architecture}
Multitiered architecture is a client–server
architecture in which presentation, application processing,
and data management functions are physically separated. Tiers are different from layers since they refer to the
physical organization on the system.

Common multitier architectures are:
\begin{description}
	\item[One-tier:] Everything happens in the same machine, easy to optimize and there are no forced control switches.
	(Fig. \ref{1-tier})
	\item[Two-tiers:] Clients have their own presentation layer, so we can differentiate presentation layers between
	clients and save system resources, allowing the resource manager to allocate resources only for the application
	layers, it also allow systems federation. (Fig. \ref{2-tier})
	\item[Three-tiers:] The application logic, presentation layer and resource manager are each on different machines.
	(Fig. \ref{3-tier})
	\item[N-tiers:] Generalization of three-tier architecture which has more tiers.
\end{description}
With three or more tiers we need a piece of software that will act as an interpreter between the application logic and
the resource manager and the client, since the application layer will receive requests from both these subsystems or
more in a N-tier architecture.

\begin{figure}[p]
	\includegraphics[width=10cm]{1-tier}
	\caption{One tier architecture}
	\label{1-tier}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=10cm]{2-tier}
	\caption{Two tier architecture}
	\label{2-tier}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=10cm]{3-tier}
	\caption{Three tier architecture}
	\label{3-tier}
\end{figure}

\begin{definition}
	Application Program Interface: An interface to invoke the system or a service of the system from the outside.
\end{definition}

\section{Middleware}

\begin{definition}
	Middleware is a level of indirection between clients and other layers of the system, which introduces an
	additional layer of business logic encompassing all underlying systems.
\end{definition}

It has two different communication primitives:
\begin{description}
	\item[Blocking interaction.] Which is an improperly synchronous interaction where the client is blocked until the
	response is received (without having a bounded time for execution hence improperly synchronous).
	\item[Non-blocking interaction.] The client can continue its execution without explicitly waiting for the response.
\end{description}

\paragraph{Blocking interaction.}
Traditionally, distributed applications use blocking calls.
Synchronous interaction requires both parties to be “alive”, the caller must wait until the response comes back.
The receiver does not need to exist at the time of the call (e.g., some technologies create an instance of the
service/server /object when called if it does not exist already).

Because it synchronizes client and server, this mode of operation has several disadvantages:
\begin{itemize}
	\item connection overhead
	\item higher probability of failures
	\item difficult to identify and react to failures
	\item it is a one-to-one system; it is not really practical for nested calls and complex interactions, which increase
	these problems
\end{itemize}
Synchronous invocations require to maintain a session between the caller and the receiver, which is expensive in terms
of CPU resources, limiting the number of active sessions. An improvement is session pooling, where a pool of sessions is
maintained active and for each request only a thread is associated with a session.
There is also the need of a context management system which will associate each client with the proper responses and
the relative data to make meaningful interactions.

A crash can create several problematic situations, because the context is lost and needs to be reinizialized:
\begin{itemize}
	\item If the client crashes before sending the request we have no problems.
	\item If the server crashes we can have three possibilities:
	\begin{itemize}
		\item the crash is before the request processing, we lose the request
		\item the crash is during the request processing, we can have inconsistencies
		\item the crash is before the response is sent, the client will not see any response but the request has been
		performed.
	\end{itemize}
\end{itemize}

To solve these problems the middleware provides two solutions:

\paragraph{1 - Enhanced support.}
\begin{description}
	\item[Transactional interactions:] which enforce the exactly one semantic for the procedure calls giving execution
	guarantees that enable more complex interactions.
	\item[Service replication and load balancing:] preventing the service from becoming unavailable in case of failure
	(does not address the failure recovery)
\end{description}

\paragraph{2 - Asynchronous interaction.}
That can take place in two forms and keep the request stored until a response is received:
\begin{description}
	\item[non-blocking invocation:] where the client does not explicitly wait for the response
	\item[persistent queues:] request and response are stored persistently in a queue until they are both accessed by
	client and server.
\end{description}

\begin{definition}
	Remote Procedure Call: hides communication details behind a procedure call and helps to bridge heterogeneous
	platforms.
\end{definition}

\begin{definition}
	Sockets: operating system level interface to the underlying communication protocols.
\end{definition}

\begin{definition}
	TCP, UDP: User Datagram Protocol (UDP) transports data packets without guarantees. Transmission Control Protocol
	(TCP) verifies correct delivery of data streams.
\end{definition}

\begin{definition}
	Internet Protocol (IP): moves a packet of data from one node to another.
\end{definition}

\paragraph{Understanding Middleware.}
Middleware can be intended as:
\begin{description}
	\item[Programming Abstraction:] to hide low level details of hardware, networks, and distribution; the trend
	is towards increasingly more powerful primitives that, without changing the basic concept of RPC, have additional
	properties or allow more flexibility in the use of the concept.
	The programming model of a middleware infrastructure shows the limitations, performance and evolution of the
	platform, giving insight on its applicability.
	\item[Infrastructure:] to provide a comprehensive platform for developing and running complex distributed systems,
	which result in service oriented architectures with standardized interfaces. Which takes into account the non
	functional properties ignored by data models (security, performance, etc.) and make development, maintenance and
	monitoring cheaper.
\end{description}

\subsection{Message oriented middleware}
Message oriented middleware (MOM) uses a two queues to communicate and acts as an interface between the parties which need
to communicate, these queues are shared and the whole infrastructure communicates with messages.
This type of interaction creates a new type of interaction, an anonymous many to many interaction.

\paragraph{Publish/Subscribe interaction.} Participants are divided in two groups:
\begin{itemize}
	\item Publishers (which send messages)
	\item Subscribers (which receive messages)
\end{itemize}
The middleware acts as the central entity which receives the messages from publishers and relays them to the
interested subscribers, the communication can be topic bases (subscribers notify their interest in a set of topics) or
content based (more accurate filters are used to check the content of the message). Notification can be given by the
middleware (push model) or the subscribers can explicitly check if there are new messages (pull model).

\subsection{Middleware on the internet}
In some cases infrastructures use a public network to connect some entities and in this case the firewall can be a
problem since they are configured to block RPCs.
To make the RPC work with firewall, thus allowing only trusted call we have several options, however the best option is
to tunnel these requests, which can involve a server, a client and the middleware (which is split between server and
client). (Fig. \ref{m2m})

\begin{figure}[ht]
	\includegraphics[width=10cm]{m2m}
	\caption{Infrastructure over a public WAN tunnelling middleware communication}
	\label{m2m}
\end{figure}

\section{Remote Procedure Calls}
RPC is a point-to-point protocol which allows two entities to communicate when there are more than two entities
communicating; each call is treated as independent (but they could be not independent), plain RPC make recovery from a
failure difficult.

To use an RPC we need client and server stubs to be created by the development environment, using the interface
definition language. This makes both client and server have the impression that the procedure is called on the local
machine. (Fig. \ref{rpc1})

\begin{figure}[ht]
	\includegraphics[width=10cm]{rpc1}
	\caption{Stubs creation}
	\label{rpc1}
\end{figure}

Then the communication can be done directly between client and server (Fig. \ref{rpc2})
\begin{figure}[ht]
	\includegraphics[width=10cm]{rpc2}
	\caption{Direct communication}
	\label{rpc2}
\end{figure}

or by relying on the middleware which can also act as a name server to client, balancing the load between the servers
and offering some execution guarantees. (Fig. \ref{rpc3})

\begin{figure}[ht]
	\includegraphics[width=10cm]{rpc3}
	\caption{Middleware-based communication}
	\label{rpc3}
\end{figure}

With RPC we can avoid to implement a complete infrastructure for every distributed application and:
\begin{itemize}
\item Hide distribution behind server procedure process calls
\item Provide an interface definition language (IDL) to describe the services
\item Generate all the additional code necessary to make a procedure call remote and to deal with all the communication
	aspects of the chosen language
\item Provide a specific binder in case it has a distributed interface name and directory service system
\end{itemize}

\subsection{Transactional RPC}
Transactional RPC provide the language to bind several call into a single atomic unit (Fig. \ref{t-rpc}), usually
includes an interface to databases to execute transactions using the 2 phase commit.

\begin{figure}[ht]
	\includegraphics[width=10cm]{t-rpc}
	\caption{Transactional RPC communication}
	\label{t-rpc}
\end{figure}

\paragraph{Two phase commit.} Uses a transaction monitor which before committing the transaction check if all
interested parties can execute the update and after receiving all the responses decides to commit or rollback the
transaction.
