/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.restserver;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 *
 * @author biar
 */
@Path("Bank")
@Produces("text/xml")
public class Bank {
    private Map<Integer,BankAccount> accounts;
    
    public Bank(){
        accounts=new HashMap<Integer,BankAccount>();
    }
    private BankAccount find_account(int id){
        return accounts.get(id);
    }
    
    @GET
    @Path("")
    public AccountEntries listAccounts(){
        Set<Entry<Integer,BankAccount>> set=accounts.entrySet();
        List<AccountEntry> res=new ArrayList<AccountEntry>();
        Iterator<Entry<Integer,BankAccount>> it=set.iterator();
        Entry<Integer,BankAccount> be;
        AccountEntry ae=null;
        while(it.hasNext()){
            be=it.next();
            ae= new AccountEntry(be.getKey(),be.getValue().toString());
            res.add(ae);
        }
        return new AccountEntries(res);
    }
    
    @GET
    @Path("{id}")
    public BankAccount getAccount(@PathParam("id")int id){
        return find_account(id);
    }
    
    @POST
    @Path("")
    public Response newAccount(BankAccount account){
        Iterator<BankAccount> it=accounts.values().iterator();
        while(it.hasNext()){
            if(account.equals(it.next())){
                return Response.status(Response.Status.CONFLICT).build();
            }
        }
        accounts.put(accounts.size()+1,account);
        return Response.ok(account).build();
    }
    
    @DELETE
    @Path("{id}")
    public Response removeAccount(@PathParam("id")int id){
        BankAccount acc=accounts.remove(id);
        if(acc==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().build();
    }
    
    @PUT
    @Path("{id}")
    public Response updateAccount(@PathParam("id") int id, BankAccount account){
        BankAccount acc=find_account(id);
        if(acc==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (acc.equals(account)) {
            return Response.notModified().build();    
        }
        List<Operation> ops=acc.getOperations();
        account.setOperations(ops);
        accounts.put(id, account);
        return Response.ok().build();
    }
    
    @GET
    @Path("{id}/operations")
    public Operations pathToOperations(@PathParam("id") int id) {
        return new Operations(find_account(id).getOperations());
    }
    
    @POST
    @Path("{id}/operations")
    public Response addOperation(@PathParam("id") int id,Operation op) throws ParseException {
         BankAccount acc=find_account(id);
        if(acc==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        System.out.println("operation to add:"+op.toString()+"found account:"+acc.toString());
        acc.addOperation(op);
        System.out.println("added");
        return Response.ok().build();
    }
}
