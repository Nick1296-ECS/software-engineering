% !TEX root = SE\ Notes.tex
\graphicspath{{./imgs/ch5/}}

\chapter{Introducing Web Services}

\begin{definition}
	A web service is a software component available on the Web, a programmatically available application logic exposed
	over the Internet. Any piece of code and any application component deployed on a system can be transformed into a
	network-available service.
	A Web service can be invoked by some other client application/component.
\end{definition}

\begin{definition}
	e-Service is the provision of a service via the Internet, meaning just about anything done online.
	Basically whichever Web application usable by a human, through a user interface.
	To build an e-service several web services may be needed.
\end{definition}

Services reflect a new “service-oriented” approach to programming, based on the idea of composing applications by
discovering and invoking network-available services rather than building new applications or by invoking available
applications to accomplish some task.

\section{Web Services}

A web service performs some encapsulated business function which can be:
\begin{itemize}
	\item a self-contained business task (e.g. funds withdrawal);
	\item a full-fledged business process (e.g. automated purchasing of supplies);
	\item an application (e.g. demand forecasts);
	\item a service-enabled resource (e.g. access to back-end database);
\end{itemize}
Web services can be mixed together to create a complete process (Fig. \ref{service}) with decreased human interaction,
this process is platform independent and can have several billing models based on the service performance and/or
subscription.

\begin{figure}[ht]
	\includegraphics[width=10cm]{service}
	\caption{A web service based process}
	\label{service}
\end{figure}

\paragraph{Application Service Provider.}
The concept of software as a service appeared for the first time in the ASP model, where an application is rented to a
client. This application is developed by in all its aspects to provide a complete solution in which the client has
little opportunity to customize it (only appearance can be changed) and it can be hosted by the developer or can be
integrated as a module in the customer's website.

\paragraph{ASP vs web services.}
The ASP model has several limitations, such as:
\begin{itemize}
	\item inability to build a highly interactive application;
	\item inability to build a complete customizable application;
	\item inability to integrate several applications;
	\item the ASP model uses monolithic application, in which modules are hard to be decoupled and reused;
\end{itemize}

On the other hand the web service model as several characteristics:
\begin{itemize}
	\item XML based;
	\item loosely coupled asynchronous interactions
	\item uses standards to make access and communication over the internet easier
\end{itemize}

Web services can be used in an enterprise to save development costs and improve reusage of the same components or
between enterprises to have a standard means to provide and access services, improving the business opportunities.

\subsection{Web services properties}
Web services can be:
\begin{itemize}
	\item Simple informational services which expose an API or have a request-response behavior;
	\item complex services which depend on pre-existent services to create multi-step business interaction;
\end{itemize}
The service is characterized by functional or non-functional properties and can be stateless or stateful; they can have
a fine (almost an atomic behavior) or coarse granularity (a complex and possibly multi-session behavior).
All web services are loose coupled, since they do not need to know how their partner in communication behaves or is
implemented.
The interaction with a web service can either be synchronous (RPC-like with simple information, e.g. only values) or
asynchronous (message-style service where documents are exchanged). In either case the interaction is well defined by
the web service description language (WSDL) which describes the rules use to interfacing and interacting with between
the web service and other applications. The implementation (Fig. \ref{realization}) of the web service is concealed by
a service interface which is described by the WSDL and can be implemented in different languages according to the
service provider, however it always describes itself and behaves in the same way, so it can be accessed by the whole
world.

\begin{figure}[ht]
	\includegraphics[width=10cm]{realization}
	\caption{Web services realized by different means (bottom) and deployed to be used by a client (top)}
	\label{realization}
\end{figure}

\subsection{Roles}
The service model uses three different roles (Fig. \ref{roles}):
\begin{description}
	\item[Service provider:] organizations that provide the service implementations, supply their service descriptions,
	and provide related technical and business support;
	\item[Service clients:] end-user organizations that use some service;
	\item[Service registry:] a searchable directory where service descriptions can be published and searched by service
	requestors to find service descriptions and obtain binding information for services. This information is sufficient
	for the service requestor to contact, or bind to, the service provider and thus make use of the services it provides.
\end{description}
In this way we can design a software system that provides services to clients (which can be other applications or end
users) that are distributed in a network via a published discoverable interface.
\begin{figure}[ht]
	\includegraphics[width=10cm]{roles}
	\caption{Interaction flow of the roles used in web services applications}
	\label{roles}
\end{figure}
These web services use several related technologies and standards that are used as a stack to provide the necessary
functions.

\begin{definition}
	QoS refers to the ability of a Web service to respond to expected invocations and perform them at the level
	commensurate to the mutual expectations of both its provider and its customers.
\end{definition}

\begin{definition}
	A SLA is a formal agreement (contract) between a provider and client, formalizing the details of use of a Web
	service and its QoS parameters (contents, price, delivery process, acceptance and quality criteria, penalties,
	etc...) in measurable terms and in a way that meets the mutual understandings and expectations of both providers and
	clients.
\end{definition}

Web services offer several advantages:
\begin{itemize}
	\item Standard way to expose legacy applications;
	\item Overcome application integration issues;
	\item standard way to develop internet native applications for every internal and external usage;
	\item standard interface for cross-enterprise specific systems, to ease business to business integration;
\end{itemize}

\section{Communication with SOAP}
Conventional distributed application use distributed communication technologies based on Object oriented RPC to couple
network protocols and object orientation (ORPC use a symbolic name to locate an object in the server process).
These technologies however require that the both ends of the communication are implemented in the same language and can
be blocked by firewalls since they have proprietary protocols that may not be considered safe in some networks.

SOAP is an XML-based communication protocol which allow different entities (implemented in different technologies) to
communicate over the network. It uses XML to code a request-response scheme that is carried over the network by HTTP
and its goal is inter application communication. (Fig. \ref{SOAP})

\begin{figure}[ht]
	\includegraphics[width=10cm]{SOAP}
	\caption{Soap based communication (RPC request/response with POST method)}
	\label{SOAP}
\end{figure}

SOAP can be used in two ways:
\begin{description}
	\item[RPC communication:] Uses XML to code both the request and the response;
	\item[Document exchanging:] The document is coded with XML and there may not be a response to a message carrying this
	document, the protocol leaves the contents of the document untouched;
\end{description}

The default binding for the SOAP protocol is the HTTP protocol, however other protocols can be used to carry SOAP messages, but
HTTP is preferred since SOAP uses the same error codes and HTTP is permitted in almost every network.
SOAP with HTTP can use both GET and POST operations, with GET only the response is a SOAP message but when POST is used
the request and the response are both SOAP messages.

Advantages of SOAP are:
\begin{itemize}
	\item Simplicity
	\item Portability
	\item Firewall friendliness
	\item Use of open standards
	\item Interoperability
	\item Universal acceptance.
\end{itemize}

Disadvantages of SOAP are:
\begin{itemize}
	\item Too much reliance on HTTP
	\item Statelessness
	\item Serialization by value and not by reference.
\end{itemize}

\section{Service description and WSDL}
Web services must be defined in a consistent manner to be discovered and used by other services and applications. Web
service consumers must determine the precise XML interface of a web service because the  XML Schema alone cannot
describe important additional details involved in communicating with a Web service.
The service description reduces the amount of required common understanding, custom programming and integration
being a machine understandable standard describing the operations of a Web service. The wire format and
transport protocol that the Web service uses to expose this functionality are described by the WSDL and it can also
describe the payload data using a type system.

\begin{definition}
	The web services description language (WSDL) is the XML-based service representation language used to describe the
	details of the complete interfaces exposed by Web services and thus is the means to accessing a Web service.
\end{definition}

In detail the WSDL uses an XML document that describes how to interact (Fig. \ref{wsdl}) with its associated web
service and it is a
"contract" between the provider and the requestor, since it binds both of them to the described interaction.

\begin{figure}[ht]
	\includegraphics[width=10cm]{wsdl}
	\caption{WSDL influence over communication}
	\label{wsdl}
\end{figure}

WSDL is an platform independent language that describes:
\begin{description}
	\item[What] a service does;
	\item[Where] it resides;
	\item[How] to invoke it;
\end{description}
WSDL documents can be separated into distinct sections (Fig. \ref{wsdl-document}):
\begin{itemize}
	\item The service-interface definition that describes the general Web service interface structure.
	Comprehends: data type, operation parameters, set of operations and action descriptions.
	This contains all the operations supported by the service, the operation parameters, and abstract data types.
	\item The service implementation part binds the abstract interface to a concrete network address, to a specific
	protocol, and to concrete data structures.
	It describes bindings operation, endpoints associated with the binding and location for each binding, it can also
	reference other XML documents.
\end{itemize}
In this way each part can be independently reused and their combination provide sufficient information for the service
requestor to use the service.

\begin{figure}[ht]
	\includegraphics[width=10cm]{wsdl-document}
	\caption{WSDL document parts and their connections}
	\label{wsdl-document}
\end{figure}

WSDL interfaces support four common types of operations that correspond to the incoming and outgoing versions of two
basic operation types:
\begin{itemize}
	\item An incoming single message passing operation and its outgoing counterpart (“one-way” and “notification”
	operations)
	\item the incoming and outgoing versions of a synchronous two-way message exchange (“request/response” and “solicit
	response”).
\end{itemize}
Any combination of incoming and outgoing operations can be included in a single WSDL interface, providing support for
both push and pull interaction models at the interface level.

\section{Registry and UDDI}
Universal Description, Discovery and Integration (UDDI) is a registry standard which maintains the web services description
and provides a registry that supports service publishing and subscription. Enabling (Fig \ref{SOA}):
\begin{itemize}
	\item service and business description;
	\item discovering of business that offer a service;
	\item integration with the desired businesses (through service usage)
\end{itemize}

\begin{figure}[ht]
	\includegraphics[width=10cm]{soa}
	\caption{UDDI registry usage}
	\label{SOA}
\end{figure}

UDDI business registration consists of three components:
\begin{description}
	\item[white pages:] contact information;
	\item[yellow pages:] classification information \\ (based on standard industry taxonomies);
	\item[green pages:] services information and technical capabilities;
\end{description}

Web services have many advantages, however they are not the solution to everything since they have several
disadvantages:
\begin{itemize}
	\item performance issues
	\item no appropriate transaction management
	\item cannot express business semantics
	\item too many unharmonized emerging standards that can compromise mission critical operations
\end{itemize}

SOAP is a complex system that uses the a stack of several components (Fig. \ref{soap-stack}), each with a different
function.

\begin{figure}
	\includegraphics[width=10cm]{soap-stack}
	\caption{Complete SOAP stack of components.}
	\label{soap-stack}
\end{figure}


\section{RESTful services}
REST is a simple protocol that transmit data over HTTP without the SOAP additional layers (Fig. \ref{rest-wsdl}), the
data is in the form of web page meant to be consumed by a program.

\begin{figure}[ht]
	\includegraphics[width=10cm]{restful-wsdl}
	\caption{WSDL and RESTful architectures comparison}
	\label{rest-wsdl}
\end{figure}
RESTful actions are formed by an URIs and an HTTP verb  (Fig. \ref{rest}).

\begin{figure}[ht]
	\includegraphics[width=10cm]{rest}
	\caption{RESTful command mapping example}
	\label{rest}
\end{figure}
RESTful interactions follow some simple basic principles:
\begin{itemize}
	\item Addressability (and URL for each resource)
	\item Uniform Interface
	\item Stateless interactions
	\item Self-describing messages
	\item Hypermedia
\end{itemize}
REST and RPC are incompatible since REST addresses end-points and RCP address software components.
Compared to SOAP, REST is way lighter but both of them provide the same functionalities.