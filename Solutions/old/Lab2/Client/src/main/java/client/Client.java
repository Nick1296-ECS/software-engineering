package client;

import org.apache.cxf.jaxrs.client.WebClient;

public class Client {
    public static void main(String[] args) throws Exception {
        WebClient client = WebClient.create("http://localhost:7070/server");
        Course course = client.path("courses/1").accept("text/xml").get().readEntity(Course.class);


        System.out.println(course.getStudents());
    }
}
