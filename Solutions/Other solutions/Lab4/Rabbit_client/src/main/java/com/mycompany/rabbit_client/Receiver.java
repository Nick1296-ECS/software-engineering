package com.mycompany.rabbit_client;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

public class Receiver {

    // Queue name to which connect
    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv) throws Exception {
        
        // Create the connection with the server through the ConnectionFactory
        // to negoziation of protocol and authentication
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.49.81"); // Server IP or name to which connect
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        // We don't care about closure because they are managed by java.io.closable

        // Create the queue through which to send the messages
        boolean durable = true; // it's the "persistence" of message in the queue
        channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        
        // Define how many messages (prefetchCount) per receive at each request 
        // from the queue
        int prefetchCount = 1;
        channel.basicQos(prefetchCount);

        // Bufferize the message that arrives from the queue
        // (it's an internal function necessary for basicConsume)
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            
            try {
                doWork(message);
            } finally {
                System.out.println(" [x] Done");
                // Send back an ack when the work is done
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            }
        };
        boolean autoAck = false; // acknowledgment is covered below

        // Read the message
        channel.basicConsume(QUEUE_NAME, autoAck, deliverCallback, consumerTag -> { });
    }
    
    // This function simulates the task: for each dot that it finds in the 
    // message, it sleep for one second (simulating the work)
    private static void doWork(String task) {
        for (char ch : task.toCharArray()) {
            if (ch == '.') {
                try {
                    Thread.sleep(1000);
                    System.out.println("---------DOING WORK----------");
                } catch (InterruptedException _ignored) {
                    Thread.currentThread().interrupt();
                }
            }
        }
  }
}