/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.softeng.restclient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author biar
 */
@XmlType( propOrder = { "id", "amount", "date" , "description"} )
@XmlRootElement( name = "Operation" )
class Operation {
    private int id;
    private double amount;
    private Date date;
    private String description;
    
    public Operation(){}
    
    public Operation(int id,double amount, String d,String desc) throws ParseException{
        this.id=id;
        this.amount=amount;
        this.date=(new SimpleDateFormat("yyyy-mm-dd")).parse(d);
        this.description=desc;
    }

    public int getId() {
        return id;
    }

    @XmlElement(name="id")
    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    @XmlElement(name="amount")
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    @XmlElement(name="date")
    public void setDate(Date date) {
        this.date = date;
    }
    
    public String getDescription() {
        return description;
    }

    @XmlElement(name="description")
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Operation{" + ", id=" + id + ", amount=" + amount + ", date=" + date + ", description=" + description + '}';
    }
}
